# Generated by Django 5.0.6 on 2024-05-13 22:44

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("recipes", "0002_recipe_description"),
    ]

    operations = [
        migrations.AlterField(
            model_name="recipe",
            name="description",
            field=models.TextField(),
        ),
    ]
