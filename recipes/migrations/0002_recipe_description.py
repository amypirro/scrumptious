# Generated by Django 5.0.6 on 2024-05-13 22:43

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("recipes", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="recipe",
            name="description",
            field=models.TextField(null=True),
        ),
    ]
