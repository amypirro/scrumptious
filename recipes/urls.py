from django.urls import path
from recipes.views import show_recipe, list_recipes, create_recipe, edit_recipe, my_recipe_list

urlpatterns = [
    path("", list_recipes, name="list_recipes"),
    path("mine/", my_recipe_list, name="my_recipe_list"),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("<int:id>/edit/", edit_recipe, name="edit_recipe"),
    path("create/", create_recipe, name="create_recipe"),
]
