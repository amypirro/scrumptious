from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/detail.html", context)


def list_recipes(request):
    recipes = Recipe.objects.all()
    return render(request, "recipes/list.html", {"recipes": recipes})


@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    return render(request, "recipes/list.html", {"recipes": recipes})


@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("list_recipes")
    else:
        form = RecipeForm()
    return render(request, "recipes/create.html", {"form": form})


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:  # GET
        form = RecipeForm(instance=recipe)
    return render(request, "recipes/edit.html", {"form": form})
